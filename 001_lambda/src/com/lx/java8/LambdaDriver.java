package com.lx.java8;

import java.util.Comparator;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;

import org.junit.Test;

/**
 Lambda表达式
 	一、“—>”操作符
  		1.在Java8中引入了一个新的操作符号“->”，该操作符被称为箭头操作符或者是Lambda操作符
  		2.左侧：Lambda表达式的参数列表
  		3.右侧：表达式所需要执行的功能，称为Lambda体。
  	二、语法格式
  		0.Lambda表达式需要使用函数式接口来接受
  		1.语法格式一：无参数，无返回值
  		2.语法格式二：有一个参数，没有返回值
  		3.语法格式三：当只有一个参数的时候，可以省略掉参数的小括号
  		4.语法格式四：有两个及以上个数参数，并且有返回值，需要使用{}把lambda体包起来
  		5.语法格式五：有返回值，但是lambda体只有一条语句时，可以省略{}和return
  		6.语法格式六：可以指明参数类型，默认情况下，可以不指定。注意的是，如果要指明，就都得指明
  	三、类型推断
  		在Lambda表达式中的参数类型可以由编译器推断得出的。Lambda 表达式中无需指定类型,程序依然可以编译,
  		这是因为 javac 根据程序的上下文,在后台 推断出了参数的类型。Lambda 表达式的类型依赖于上 下文环境,
  		是由编译器推断出来的。这就是所谓的 “类型推断”
 * @author liuxing
 */
public class LambdaDriver {

	@Test
	public void test1() {
		Runnable r = () -> System.out.println("Hello,Lambda");
		r.run();
	}

	@Test
	public void test2() {
		Consumer<String> c = (x) -> System.out.println("Hello," + x);
		c.accept("Baby");
	}

	@Test
	public void test3() {
		Consumer<String> c = x -> System.out.println("Hello," + x);
		c.accept("Test3");
	}

	@Test
	public void test4() {
		Comparator<Integer> c = (x, y) -> {
			System.out.println("compare");
			return Integer.compare(x, y);
		};
	}

	@Test
	public void test5() {
		BinaryOperator<Long> op = (x, y) -> x + y;
	}

	public void test6() {
		BinaryOperator<Integer> op = (Integer x, Integer y) -> x + y;
	}
}
