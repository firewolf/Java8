package com.lx.java8;

public class Car {
	private String name;
	private int price;

	public Car() {
		// TODO Auto-generated constructor stub
	}

	public Car(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Car [name=" + name + ", price=" + price + "]";
	}

	public static void showInfo(String s) {
		System.out.println("I am a Car, My name is " + s);
	}
}
