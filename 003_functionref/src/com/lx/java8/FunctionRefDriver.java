package com.lx.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.junit.Test;

/**
 * 方法引用、构造器引用
	1.构造器引用
   	  Class::new
   	2.静态方法引用
   	  Class::method
   	3.所有对象任意方法引用
   	  Class:method
   	4.特定对象的方法引用
   	  instance:method
 * @author liuxing
 *
 */
public class FunctionRefDriver {

	@Test
	public void test1() {
		//无参构造器
		Supplier<Car> c = Car::new;
		Car car = c.get();
		System.out.println(car);

		//带参数的构造器
		Function<String, Car> f = Car::new;
		Car c2 = f.apply("宝马");
		System.out.println(c2);

	}

	@Test
	public void test2() {
		//静态方法引用
		Consumer<String> con = Car::showInfo;
		con.accept("奔驰");
	}

	@Test
	public void test3() {
		//方法引用
		Consumer<String> s = System.out::println;
		s.accept("你好");
	}

	@Test
	public void test4() {
		List<String> names = new ArrayList<>();
		names.add("Google");
		names.add("Runoob");
		names.add("Taobao");
		names.add("Baidu");
		names.add("Sina");
		names.forEach(System.out::println);
	}
}
