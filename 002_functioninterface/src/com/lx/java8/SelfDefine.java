package com.lx.java8;

/**
 * 自定义函数式接口
 * @author liuxing
 *
 */
//加上这个注解，帮助我们检查是不是函数式接口，特别是方法比较多的时候，比较实用
@FunctionalInterface 
public interface SelfDefine<T, U> {
	public T handleValue(U u);
}
