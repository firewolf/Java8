package com.lx.java8;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

import org.junit.Test;

public class DateTimeDriver {

	/**
	 * LocalDate：日期
	 * LocalTime：时间
	 * LocalDateTime：日期+日期
	 */
	@Test
	public void localdatatime() {
		//当前日期
		LocalDate ld = LocalDate.now();
		System.out.println("当前日期：" + ld);

		//当前时间
		LocalTime lt = LocalTime.now();
		System.out.println("当前时间：" + lt);

		//当前日期时间
		LocalDateTime ldt = LocalDateTime.now();
		System.out.println("当前日期和时间：" + ldt);

		//根据指定的数据来构造日期时间
		LocalDateTime mldt = LocalDateTime.of(2018, 7, 12, 9, 23, 34);
		System.out.println("自己构造的日期：" + mldt);

		//2年后的日期是
		LocalDate nld = ld.plusYears(2);
		System.out.println("两年后的日期：" + nld);

		//2天后的日期
		LocalDate nld2 = ld.plusDays(2);
		System.out.println("两天后的日期：" + nld2);

		//两天前的日期是
		LocalDate nld3 = ld.minusDays(2);
		System.out.println("两天前的日期：" + nld3);

		//当月天数
		int dayC = ld.getDayOfYear();
		System.out.println("这是今年的第：" + dayC + "天");

	}

	/**
	 * Instance：用于“时间戳”的运算
	 */
	@Test
	public void instance() {
		Instant now = Instant.now();
		System.out.println("当前时间是：" + now);
		int nano = now.getNano();
		System.out.println("当前的纳秒时间是：" + nano);

		Instant now1 = now.minusSeconds(10);
		System.out.println("10秒前：" + now1);

		Instant now2 = now.plusSeconds(10);
		System.out.println("10秒后：" + now2);
	}

	/**
	 * Duration:两个时间的间隔
	 * Period:两个日期的间隔
	 */
	@Test
	public void period() {
		LocalDate d1 = LocalDate.now();
		LocalDate d2 = LocalDate.of(2017, 9, 29);
		Period p = Period.between(d1, d2);
		int days = p.getDays();
		System.out.println("日期间隔是：" + days + "天");

		LocalTime t1 = LocalTime.of(0, 0, 0);
		LocalTime t2 = LocalTime.now();
		Duration d = Duration.between(t1, t2);
		System.out.println("时间间隔是：" + d.getSeconds());
	}

	/**
	 * 时间矫正器
	 */
	@Test
	public void timeAdjust() {
		LocalDate l = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.WEDNESDAY));
		System.out.println("下个周三是：" + l);
	}

	/**
	 * 时间日期解析
	 */
	@Test
	public void parse() {
		//预置的格式
		DateTimeFormatter fmt1 = DateTimeFormatter.BASIC_ISO_DATE;
		String dt1 = fmt1.format(LocalDateTime.now());
		System.out.println(dt1);

		DateTimeFormatter fmt2 = DateTimeFormatter.ISO_DATE_TIME;
		String dt2 = fmt2.format(LocalDateTime.now());
		System.out.println(dt2);

		//自定义格式
		DateTimeFormatter fmt3 = DateTimeFormatter.ofPattern("yyyy-MM-dd mm:HH:ss");
		String dt3 = fmt3.format(LocalDateTime.now());
		System.out.println(dt3);
	}

	/**
	 * 转化
	 */
	@Test
	public void convert() {
		LocalDate d = LocalDate.now();
		Date from = Date.from(Instant.now());
		System.out.println(from);
	}
}
