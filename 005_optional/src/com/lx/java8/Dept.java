package com.lx.java8;

/**
 * 部门信息
 * @author liuxing
 *
 */
public class Dept {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
