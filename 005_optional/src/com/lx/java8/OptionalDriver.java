package com.lx.java8;

import java.util.Optional;

import org.junit.Test;

public class OptionalDriver {
	@Test
	public void testOptional() {
		//需求：获取一个Employee对象，然后打印他所在部门的部门名字的长度
		Employee e = new Employee(); //这里其实应该是业务获取的，当然也有可能得到的是null
		//为了避免空指针，需要如下编写代码
		if (e != null && e.getDept() != null && e.getDept().getName() != null) {
			System.out.println(e.getDept().getName().length());
		}

		//使用java8的option，可以如下编写代码
		Optional.of(e).map(x -> x.getDept()).map(x -> x.getName()).ifPresent(System.out::println);
		//获取如下编写
		Optional.of(e).map(Employee::getDept).map(Dept::getName).ifPresent(System.out::println);
		//这样就避免了繁琐的为空判断
	}
	
	
}
