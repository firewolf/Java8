package com.lx.java8;

/**
 * 员工信息
 * @author liuxing
 *
 */
public class Employee {
	private String ename;
	private Dept dept;
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public Dept getDept() {
		return dept;
	}
	public void setDept(Dept dept) {
		this.dept = dept;
	}
	
}
